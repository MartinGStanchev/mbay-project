import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import userRoute from './routes/userRoute.js';
import passport from 'passport';
import { sessionSecret, sessiosExpires } from './config/config';
import session from 'express-session'
import cookieParser from 'cookie-parser';

const app = express();
const router = express.Router()
/**
 * Connect to the database
 */




mongoose.connect('mongodb://localhost');
mongoose.set('useFindAndModify', false);

let corsOptions = {
    origin: "http://localhost:3000",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true,
}
/**
 * Middleware
 */
const sess = {
    resave: false,
    saveUninitialized: false,
    secret: sessionSecret,
    proxy: false,
    name: "sessionId",
    maxAge: Date.now() + parseInt(sessiosExpires),
}
app.use(cors(corsOptions))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser(sessionSecret));
app.use(session(sess))
app.use(passport.initialize());
app.use(passport.session());
import './config/passportController';
app.use('/api/v1', router);

// 
// catch 400
app.use((err, req, res, next) => {
    console.log(err.stack);
    res.status(400).send(`Error: ${res.originUrl} not found`);
    next();
});

// catch 500
app.use((err, req, res, next) => {
    console.log(err.stack)
    res.status(500).send(`Error: ${err}`);
    next();
});

/**
    * Register the routes
    */

userRoute(router);

export default app;