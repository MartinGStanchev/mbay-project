import User from '../../models/userModel.js';

exports.getAllUsers = (req, res) => {
    User.find({}, (err, users) => {
        if (err) {
            res.send(err);
        }

        res.json(users);
    });
};


exports.updateUser = (req, res) => {
    User.findOne({ _id: req.user._id }, (err, user) => {
        console.log(req.body)
        Object.keys(req.body).forEach(key => {
            if (Array.isArray(user[key])) {
                if (req.body[key].isDefault) user[key].forEach(el => el.isDefault = false);
                user[key].push(req.body[key]);
            } else {
                user[key] = req.body[key];
            }
        })
        if (err) {
            console.log(err)
            res.send(err);
        }
        console.log(user)
        user.save(error => {
            if (error) console.log(error)
            res.json(user);
        })
    });
};

exports.deleteUser = (req, res) => {
    User.remove({
        _id: req.params.userId
    }, (err) => {
        if (err) {
            res.send(err);
        }

        res.json({
            message: `User ${req.params.userId} successfully deleted`
        });
    });
}