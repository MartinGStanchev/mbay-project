import User from '../../models/userModel.js';
import jwt from 'jsonwebtoken';
import { tokenKey, sessiosExpires, sessionSecret } from '../../config/config.js'
import passport from 'passport';

exports.isAdmin = (req, res, next) => {
    /////ADD logic for cheking if it's admin;
}

exports.login = (req, res, next) => {
    passport.authenticate("local", { successRedirect: '/' }, (error, user) => {
        if (error || !user) {
            return res.status(400).json({ message: error });
        }
        let payload = JSON.parse(JSON.stringify(user));
        delete payload.password;
        /** assigns payload to req.user */
        req.login(payload, (error) => {
            if (error) {
                console.log(error)
                res.status(400).json({ message: error });
            }

            /** generate a signed json web token and return it in the response */
            const token = jwt.sign(JSON.stringify(payload), tokenKey);
            /** assign our jwt to the cookie */
            res
                .cookie('jwt', token, { httpOnly: true, secure: false })
                .status(200)
                .json({ payload });
        });
    },
    )(req, res, next);
};

exports.checkAuth = (req, res) => {
    User.findById(req['user'], (err, payload) => {
        if (err || !payload) {
            return res.status(400).json({ message: "You must login first!" });
        }
        payload = JSON.parse(JSON.stringify(payload)) ///getting rid of User prototype
        delete payload.password; ///so I can delete the password and send it
        return res.json({ payload })
    });
}

exports.logout = (req, res) => {
    req.logout();
    res.json({ info: 'success' })
};

exports.createUser = (req, res, next) => {
    const newUser = new User(req.body);
    newUser.save((err, user) => {
        if (err) {
            console.log(err)
            return res.send(err);
        }
        passport.authenticate("local", (error, user) => {
            if (error || !user) {
                return res.status(400).json({ error });
            }
            /** This is what ends up in our JWT */
            const payload = {
                _id: user._id,
                email: user.email,
                expires: Date.now() + parseInt(sessiosExpires),
            };

            /** assigns payload to req.user */
            req.login(payload, (error) => {
                if (error) {
                    console.log(error)
                    res.status(400).send({ error });
                }

                /** generate a signed json web token and return it in the response */
                const token = jwt.sign(JSON.stringify(payload), tokenKey);

                /** assign our jwt to the cookie */
                res
                    .cookie('jwt', token, { maxAge: new Date(parseInt(sessiosExpires) + Date.now()), httpOnly: true, secure: false })
                    .status(200)
                    .send({ payload });
            });
        },
        )(req, res, next);
    });
};