import auth from './authController';
import profile from './profileController';
import common from './userController';

export {
    auth,
    profile,
    common
}