
import { tokenKey } from './config';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JWTStrategy} from 'passport-jwt';
import User from '../models/userModel';

passport.serializeUser(function (user, done) {
  done(null, {
    _id: user._id,
    email: user.email
  });
});
passport.deserializeUser(function (_id, done) {
  User.findById(_id, function (err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy({
  usernameField: 'email'
}, (email, password, done) => {
  try {
    User.findOne({ email }, (err, user) => {
      if (err) {
        return done(null, err)
      }
      if (user) {
        const passwordsMatch = user.comparePassword(password, user.password);

        if (passwordsMatch) {
          return done(null, user);
        } else {
          return done('Incorrect Username / Password');
        }
      } else {
        return done("Incorrect Username")
      }
    })
  } catch (error) {
    console.log(error)
    done(error);
  }
}));

passport.use(new JWTStrategy({
  jwtFromRequest: req => req.cookies,
  secretOrKeyProvider: tokenKey
},
  (jwtPayload, done) => {
    if (Date.now() < jwtPayload.expires) {
      return done('jwt expired');
    }

    return done(null, jwtPayload);
  }
));

