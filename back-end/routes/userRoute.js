import {auth, profile, common} from '../controllers/user/index';

export default (app) => {
    app.route('/users')
        .get(common.getAllUsers)
        .post(auth.createUser)
        .put(common.updateUser)
    app.route('/login')
        .post(auth.login);
    app.route('/logout')
        .get(auth.logout)
    app.route('/checkAuth')
        .get(auth.checkAuth)
    app.route('/isAdmin/:userId')
        .get(auth.isAdmin)
    app.route('/users/:userId')
        .put(common.updateUser)
        .delete(common.deleteUser);
};