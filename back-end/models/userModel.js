import mongoose, {
    Schema
} from 'mongoose';
import bcrypt from 'bcrypt';
import { strict } from 'assert';


const AddressSchema = new Schema({
    address: {type: String},
    city: {type: String},
    state: {type: String},
    zip: {type: String},
    isDefault: {type: Boolean, default: false}
});

const PaymentMethodsSchema = new Schema({
    cardNumber: {type: String},
    cardName: {type: String},
    cvv: {type: Number, min:100, max:999},
    expirationMonth: {type: Number, min:1, max:12},
    expirationYear: {type: Number, min:19}
})
/**
 * Create database scheme for users
 */
const UserSchema = new Schema({
    email: {
        type: String,
        min: 3,
        unique: true,
        trim: true,
        match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
        required: true
    },
    password: {
        type: String,
        min: 3,
        required: true,
    },
    firstName: {
        type: String,
        min: 2
    },
    lastName: {
        type: String,
        min: 2
    },
    phoneNumber: {
        type: String,
    },
    addresses: [AddressSchema],
    paymentMethods: [PaymentMethodsSchema],
    activated: {
        type: Boolean,
        default: false
    },
    admin: {
        type: Boolean,
        default: false
    }
});

AddressSchema.index({isDefault: 1}, {unique: true, partialFilterExpression: {isDefault: true}});


UserSchema.pre('save', function(next){
    let user = this;
    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.hash(user.password, 10).then(hashedPassword => {
        user.password = hashedPassword;
        next();
    })
}), function (err) {
    next(err);
}

UserSchema.methods.comparePassword = function(candidatePassword){
    return bcrypt.compareSync(candidatePassword, this.password)
}
export default mongoose.model('User', UserSchema);