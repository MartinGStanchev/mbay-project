import api from '../helpers/request';


const loginUser = userObj => ({
  type: 'LOGIN_USER',
  payload: userObj
})

const logout = () => ({
  type: 'LOGOUT_USER'
})

export const userPostFetch = user => {
  return dispatch => api.post('/users', user)
      .then(data => {
        if (data.message) {
          // Here you should have logic to handle invalid creation of a user.
        } else {
          dispatch(loginUser(data.payload))
        }
      })
}


export const logoutUser = () => {
  return dispatch => api.get('/logout').then(() => {
    dispatch(logout())
  });
}


export const userLoginFetch = user => {
  return dispatch => api.post('/login', user)
    .then(data => {
      if (data.message) {
        // Here you should have logic to handle invalid login credentials.
      } else {
        dispatch(loginUser(data.payload))
      }
    })
}

export const checkAuth = () => {
  return dispatch => api.get('/checkAuth')
    .then(data => {
      if (data.message) {
      } else {
        dispatch(loginUser(data.payload))
      }
    })
}

export const updateUser = user => ({
  type: 'UPDATE_USER',
  url:  api.put('/users', user)
});