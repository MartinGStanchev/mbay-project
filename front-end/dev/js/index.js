import React from 'react';
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import allReducers from './reducers';
import App from './components/App';
import { createBrowserHistory } from "history";

import 'bootstrap/dist/css/bootstrap.min.css';
const history = createBrowserHistory()


const logger = createLogger();
const store = createStore(
    allReducers,
    applyMiddleware(thunk, promise, logger)
);
ReactDOM.render(
    <Provider store={store}>
            <Router history={history}>
                <App />
            </Router>
    </Provider>,
    document.getElementById('root')
);
