import React from "react";
import { connect } from 'react-redux';
import { Redirect, Route, withRouter } from "react-router-dom";

const PrivateRoute = ({ component: Component, currentUser, ...rest }) => {

    return (
        <Route
            {...rest}
            render={props => currentUser._id ? (
                <Component {...props} />
            ) : (
                    <Redirect
                        to='/login'
                    />
                )
            }
        />
    )
}

const mapStateToProps = (state) => {
    return ({ currentUser: state.users.currentUser })
};
export default withRouter(connect(mapStateToProps)(PrivateRoute));
