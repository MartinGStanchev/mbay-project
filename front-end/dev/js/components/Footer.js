import React, { Component } from 'react';


class Footer extends Component {

    render() {
        return (
            <footer className="footer-distributed">

                <div className="footer-left">

                    <h3>M<span>bay</span></h3>

                    <p className="footer-links">
                        <a href="#" className="link-1">Home</a>

                        <a href="#">About</a>

                        <a href="#">Faq</a>

                        <a href="#">Contact</a>
                    </p>

                    <p className="footer-company-name">Merchant Bay © 2019</p>
                </div>

                <div className="footer-center">

                    <div>
                        <i className="fa fa-map-marker"></i>
                        <p><span>123 Random Address</span> Sofia, Bulgaria</p>
                    </div>

                    <div>
                        <i className="fa fa-phone"></i>
                        <p>+359 896 645 133</p>
                    </div>

                    <div>
                        <i className="fa fa-envelope"></i>
                        <p><a href="mailto:martinstancehv02@gmail.com">martinstanchev02@gmail.com</a></p>
                    </div>

                </div>

                <div className="footer-right">

                    <p className="footer-company-about">
                        <span>About the company</span>
                        Merchant Bay is a place to sell and buy all you need in life even hapiness!
				</p>

                    <div className="footer-icons">

                        <a href="#"><i className="fa fa-facebook"></i></a>
                        <a href="#"><i className="fa fa-twitter"></i></a>
                        <a href="#"><i className="fa fa-linkedin"></i></a>
                        <a href="#"><i className="fa fa-github"></i></a>

                    </div>

                </div>

            </footer>
        )
    }
}
export default Footer;