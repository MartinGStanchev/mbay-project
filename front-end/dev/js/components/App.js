
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { checkAuth } from '../actions/index';
import Signup from '../containers/SignUp';
import Login from '../containers/Login';
import Home from '../containers/Home';
import NavbarInstance from '../containers/Navbar';
import Profile from '../containers/Profile/Profile';
import Footer from './Footer';
import PrivateRoute from '../helpers/privateRoute';

require('../../scss/style.scss');
require('../../scss/footer.scss')

class App extends Component {
    componentWillMount = () => {
        this.props.checkAuth();
    }

    render() {
        return (
            <div>
                <NavbarInstance></NavbarInstance>
                <div id="content-container" className="container mt-5 mb-5">
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/signup' component={Signup} />
                        <Route path='/login' component={Login} />
                        <Route path='/profile' component={Profile}/>

                        {/* Add bellow for private Route uncomment when production comes */}
                        {/* {this.props.currentUser._id ? [
                            <PrivateRoute key='/profile' path='/profile' component={Profile}/>,
                        ]: ""} */}
                    </Switch>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return ({ currentUser: state.users.currentUser })
};
const mapDispatchToProps = dispatch => ({
    checkAuth: () => dispatch(checkAuth())
})
export default connect(mapStateToProps, mapDispatchToProps)(App);