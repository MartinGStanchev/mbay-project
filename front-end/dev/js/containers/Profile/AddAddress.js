import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../../actions/index';

class AddAddress extends Component {
    componentDidMount(){
        ///get available states and cities\
        this.state = {
            addresses: {}
        }
    }

    handleChange = event => {
        this.setState({
           addresses: {
               ...this.state.addresses,
               [event.target.name]: event.target.type == 'checkbox' ? event.target.checked : event.target.value
           }
        });
    }

    handleSubmit = event => {
        event.preventDefault()
        this.props.updateUser(this.state)
    }

    render() {
        return (
            <div className="row">
                <div className="bg-white rounded-lg shadow-sm p-5">
                    <form onSubmit={this.handleSubmit} action="POST">
                        <div className="form-group">
                            <label htmlFor="inputAddress">Address</label>
                            <input type="text" className="form-control" onChange={this.handleChange} name="address" id="inputAddress" placeholder="1234 Main St" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="inputCity">City</label>
                                <input type="text" className="form-control" onChange={this.handleChange} name="city" id="inputCity" />
                            </div>
                            <div className="form-group col-md-4">
                                <label htmlFor="inputState">State</label>
                                <select name="state" id="inputState" onChange={this.handleChange} className="form-control">
                                    <option defaultValue>Choose...</option>
                                    <option>...</option>
                                </select>
                            </div>
                            <div className="form-group col-md-2">
                                <label htmlFor="inputZip">Zip</label>
                                <input type="text" className="form-control" onChange={this.handleChange} name="zip" id="inputZip" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <input className="form-check-input" onChange={this.handleChange} type="checkbox" name="isDefault" id="gridCheck" />
                                <label className="form-check-label" htmlFor="gridCheck">
                                    Use as default address.
                                </label>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary">Add Address</button>
                    </form>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({ 
    currentUser: state.users.currentUser 
});
const mapDispatchToProps = dispatch => ({
    updateUser: userInfo => dispatch(updateUser(userInfo))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAddress);