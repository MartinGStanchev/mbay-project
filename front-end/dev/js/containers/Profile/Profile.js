import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { userLoginFetch } from '../../actions/index';

import AddAddress from './AddAddress';
import PaymentInfo from './PaymentInfo'

require('../../../scss/profile.scss')

class Profile extends Component {
    constructor() {
        super()
        this.state = {
            paymentInfo: true,
            activeComponent: 'paymentInfo'
        }
    }

    renderPage = (e) => {
        this.setState({
            [this.state.activeComponent]: false,
            [e.target.value]: [e.target.checked],
            activeComponent: [e.target.value]
        })
    }
    render() {
        const { addAddress, paymentInfo } = this.state;
        return (
            <div className="row">
                <div className="col-md-4 mb-4">
                    <ul className="list-group">
                        <li className='list-group-item list-group-item-action'>
                            <label htmlFor='summary'>Summary</label>
                            <input type='radio' onChange={this.renderPage} name='chooseMenu' value="Summary" id='summary' />
                        </li>
                        <li className='list-group-item list-group-item-action'>
                            <label htmlFor='addAddress'>Add Address</label>
                            <input type='radio' onChange={this.renderPage} name='chooseMenu' value="addAddress" id='addAddress' />
                        </li>
                        <li className='list-group-item list-group-item-action'>
                            <label htmlFor='orderHistory'>Order History</label>
                            <input type='radio' onChange={this.renderPage} name='chooseMenu' value="OrderHistory" id='orderHistory' />
                        </li>
                        <li className='list-group-item list-group-item-action'>
                            <label htmlFor='paymentInfo'>Payment Info</label>
                            <input type='radio' onChange={this.renderPage} name='chooseMenu' value="paymentInfo" id='paymentInfo' />
                        </li>
                    </ul>
                </div>
                <div className="col-md-8" id="modalContainer">
                    {addAddress ? <AddAddress /> : null}
                    {paymentInfo ? <PaymentInfo /> : null}

                </div>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    userLoginFetch: userInfo => dispatch(userLoginFetch(userInfo))
})

export default connect(null, mapDispatchToProps)(Profile);