import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateUser } from '../../actions/index';

class PaymentInfo extends Component {
    constructor() {
        super()
        this.state = {
            paymentMethods: {}
        }
    }
    validateCreditCard = cardno => {
        cardno = cardno.replace(/ /g, '');
        const aexpress = /^(?:3[47][0-9]{13})$/; ///american express regex
        const visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/; ///visa regex
        const mastercard = /^(?:5[1-5][0-9]{14})$/; ///mastercard regex
        const discover = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/; ///discover regex
        const dinersClub = /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/; ///diners club regex
        const jcbCard = /^(?:(?:2131|1800|35\d{3})\d{11})$/; /// jcb regex

        if (cardno.match(aexpress) || cardno.match(visa) || cardno.match(mastercard) || cardno.match(discover) || cardno.match(dinersClub) || cardno.match(jcbCard)) {
            return true;
        }
        return false;
    }
    handleChange = e => {
        if (e.target.name == 'cardNumber') {
            e.target.value = e.target.value.replace(/[^\d]/g, '').replace(/(.{4})/g, '$1 ').trim(); ///allow only numbers in card number field and add interval every four numbers
        }
        this.setState({
            paymentMethods: {
                ...this.state.paymentMethods,
                [e.target.name]: e.target.value
            }
        });
    }
    handleSubmit = e => {
        e.preventDefault();
        if (this.validateCreditCard(this.state.paymentMethods.cardNumber)) {
            ///submit
            this.props.updateUser(this.state)
        } else {
            console.log('Invalid')
            ///handle error
            console.log(e.target)
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="mx-auto">
                        <div className="bg-white rounded-lg shadow-sm p-5">
                            <ul role="tablist" className="nav bg-light nav-pills rounded-pill nav-fill mb-3">
                                <li className="nav-item">
                                    <a data-toggle="pill" href="#nav-tab-card" className="nav-link active rounded-pill">
                                        <i className="fa fa-credit-card"></i>
                                        Credit Card
                            </a>
                                </li>
                                <li className="nav-item">
                                    <a data-toggle="pill" href="#nav-tab-paypal" className="nav-link rounded-pill">
                                        <i className="fa fa-paypal"></i>
                                        Paypal
                            </a>
                                </li>
                                <li className="nav-item">
                                    <a data-toggle="pill" href="#nav-tab-bank" className="nav-link rounded-pill">
                                        <i className="fa fa-university"></i>
                                        Bank Transfer
                             </a>
                                </li>
                            </ul>

                            <div className="tab-content">
                                <div id="nav-tab-card" className="tab-pane fade show active">
                                    {/* <p className="alert alert-success">Some text success or error</p> */}
                                    <form role="form" onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <label htmlFor="cardName">Full name (on the card)</label>
                                            <input onChange={this.handleChange} type="text" name="cardName" placeholder="Jason Doe" required className="form-control" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="cardNumber">Card number</label>
                                            <div className="input-group">
                                                <input onChange={this.handleChange} type="text" name="cardNumber" placeholder="Your card number" className="form-control" required />
                                                <div className="input-group-append">
                                                    <span className="input-group-text text-muted">
                                                        <i className="fa fa-cc-visa mx-1"></i>
                                                        <i className="fa fa-cc-amex mx-1"></i>
                                                        <i className="fa fa-cc-mastercard mx-1"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-8">
                                                <div className="form-group">
                                                    <label><span className="hidden-xs">Expiration</span></label>
                                                    <div className="input-group">
                                                        <input onChange={this.handleChange} type="number" placeholder="MM" name="expirationMonth" className="form-control" required />
                                                        <input onChange={this.handleChange} type="number" placeholder="YY" name="expirationYear" className="form-control" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="form-group mb-4">
                                                    <label data-toggle="tooltip" title="Three-digits code on the back of your card">CVV
                                                <i className="fa fa-question-circle"></i>
                                                    </label>
                                                    <input onChange={this.handleChange} type="text" required className="form-control" name="cvv" />
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" className="subscribe btn btn-primary btn-block rounded-pill shadow-sm"> Add Card  </button>
                                    </form>
                                </div>

                                <div id="nav-tab-paypal" className="tab-pane fade">
                                    <p>Paypal is easiest way to pay online</p>
                                    <p>
                                        <button type="button" className="btn btn-primary rounded-pill"><i className="fa fa-paypal mr-2" aria-hidden="true"></i> Log into my Paypal</button>
                                    </p>
                                    <p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>



                                <div id="nav-tab-bank" className="tab-pane fade">
                                    <h6>Bank account details</h6>
                                    <dl>
                                        <dt>Bank</dt>
                                        <dd> THE WORLD BANK</dd>
                                    </dl>
                                    <dl>
                                        <dt>Account number</dt>
                                        <dd>7775877975</dd>
                                    </dl>
                                    <dl>
                                        <dt>IBAN</dt>
                                        <dd>CZ7775877975656</dd>
                                    </dl>
                                    <p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapDispatchToProps = dispatch => ({
    updateUser: userInfo => dispatch(updateUser(userInfo))
});
export default connect(null, mapDispatchToProps)(PaymentInfo);