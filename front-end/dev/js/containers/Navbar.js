
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logoutUser} from '../actions/index';
import { Profile } from './Profile/Profile';
import { Redirect } from 'react-router-dom';


class NavbarInstance extends Component {
    constructor(){
        super()
        this.state = {
            redirect: false
        }
    }

    handleLogout = (e) => {
        e.preventDefault()
        this.props.logoutUser().then(() => this.setState({redirect: true}));
    }
    render() {
        let user = this.props.currentUser._id;
        let {redirect} = this.state;
        return (
            <Navbar bg="light" expand="lg">
                {redirect ? <Redirect to='login'/> : ""}
                <Navbar.Brand href="/">MBay</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {!user
                        ?   [<Nav.Link key="1" href='login'>Login</Nav.Link>,
                            <Nav.Link key="2" href="signup">Sign Up</Nav.Link>]
                        : <Nav.Link href='#' onClick={this.handleLogout}>Logout</Nav.Link>}

                        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            {user ? <NavDropdown.Item href="/profile">Profile</NavDropdown.Item> : ""}
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}
const mapState = state => {
    return {
        currentUser: state.users.currentUser
    }
}

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
  })
export default connect(mapState, mapDispatchToProps)(NavbarInstance);