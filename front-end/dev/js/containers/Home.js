import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLoginFetch } from '../actions/index';
import { Carousel, Container } from 'react-bootstrap';

class Home extends Component {
    constructor() {
        super()
        this.state = {
            slideshow_products: [],
            carouselSize: 10
        }
    }

    getProducts = () => {
        fetch('https://picsum.photos/1920/720')
            .then(product => {
                this.setState({
                    slideshow_products: [...this.state.slideshow_products,
                    {
                        img: product.url,
                        title: 'Random Picture',
                        description: 'Random Pircture Description'
                    }
                    ]
                })
            }).catch(err => {
                console.log(err)
            })

    }
    componentWillMount() {
        for (let index = 0; index <= this.state.carouselSize; index++) {
            this.getProducts()
        }
    }
    render() {
        return (
            <div className='carousel'>
                <Carousel interval='2000'>
                    {this.state.slideshow_products.map((product, index) => {
                        return <Carousel.Item key={index}>
                            <img
                                className="d-block w-100"
                                src={product.img}
                                alt={index + " slide"}
                            />
                            <Carousel.Caption>
                                <h3>{product.title}</h3>
                                <p>{product.description}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    })}
                </Carousel>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    userLoginFetch: userInfo => dispatch(userLoginFetch(userInfo))
})

export default connect(null, mapDispatchToProps)(Home);
