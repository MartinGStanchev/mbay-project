import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLoginFetch } from '../actions/index';
import { Form, Button, Card, Modal } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';


class Login extends Component {
    constructor() {
        super();
        this.state = {
            redirect: false,
            email: "",
            password: "",
            rememberMe: "false"
        }

    }
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    handleSubmit = event => {
        event.preventDefault()
        this.props.userLoginFetch(this.state).then(res => {
            this.setState({redirect: true})
        })
    }

    render() {
        const { redirect } = this.state;
        if(redirect){
            return <Redirect to='/profile'/>;
        }
        return (
            <Modal.Dialog aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Card>
                    <Card.Body>
                        <Card.Title>Login</Card.Title>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" name="email" onChange={this.handleChange}
                                    value={this.state.email}
                                    placeholder="Enter email" />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="password" placeholder="Password" value={this.state.password}
                                    onChange={this.handleChange} />
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Check type="checkbox" name="rememberMe" label="Remember me!" />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                        <Card.Link href="#">Forgot Password?</Card.Link>
                        <Card.Link href="/signup">Create new account!</Card.Link>
                    </Card.Body>
                </Card>
            </Modal.Dialog>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    userLoginFetch: userInfo => dispatch(userLoginFetch(userInfo))
})

export default connect(null, mapDispatchToProps)(Login);

/*



*/